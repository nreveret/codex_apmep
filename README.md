# Présentation de CodEx aux journées Nationales de l'APMEP 2024

Vous trouverez dans ce dépôt les fichiers utilisés lors de la présentation du lundi 21 octobre 2024.

Vous y trouverez en particulier :

* [le diaporama de la présentation](codex_apmep.pdf) ;
* les condensés des programmes d'algorithmique au lycée général :
  
  * [En seconde](/programmes_maths/seconde_algos.pdf) ;
  * [En spé. de première](/programmes_maths/premiere_spe_algos.pdf) ;
  * [En spé. de terminale](/programmes_maths/terminale_spe_algos.pdf);

* [le parcours de découverte des listes](/parcours/parcours_decouverte_ listes.pdf).

Et bien entendu : [le site de CodEx](https://codex.forge.apps.education.fr/) !