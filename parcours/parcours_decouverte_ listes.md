# Parcours pour la découverte des listes

## Prérequis

* [Fonctions simples](https://codex.forge.apps.education.fr/exercices/fonctions/)

* [Autour de range](https://codex.forge.apps.education.fr/exercices/range/)

## Accéder aux éléments d'une liste

```pycon
>>> nombres = [5, 6, 7]
>>> nombres[1]
6
```

* [La course cycliste (I)](https://codex.forge.apps.education.fr/exercices/course_cycliste/)

## Modifier une liste

```pycon
>>> nombres = [5, 6, 7]
>>> nombres[1] = 999
>>> nombres
[5, 999, 7]
```

* [Recadrer](https://codex.forge.apps.education.fr/exercices/recadrer/)


## Créer une liste en extension

```pycon
>>> nombres = [5, 6, 7]
>>> nombres.append(8)
>>> nombres
[5, 6, 7, 8]
>>> nombres = nombres + [9]
>>> nombres
[5, 6, 7, 8, 9]
```

* [Liste des termes d'une suite mathématique](https://codex.forge.apps.education.fr/exercices/termes_d_une_suite/)

* [Diviseurs d'un entier positif](https://codex.forge.apps.education.fr/exercices/diviseurs/)

* [Suite de Syracuse](https://codex.forge.apps.education.fr/exercices/syracuse/)


## Créer une liste en compréhension

```pycon
>>> nombres = [k for k in range(6, 8)]
>>> nombres
[5, 6, 7]
```

* [Liste des termes d'une suite arithmétique](https://codex.forge.apps.education.fr/en_travaux/suite_arithm/#construction-par-compr%C3%A9hension)

* [Somme des termes d'une suite mathématique](https://codex.forge.apps.education.fr/exercices/somme_suite/)

## Parcourir une liste

```pycon
>>> nombres = [5, 6, 7]
>>> for i in range(3):
...     print(nombres[i])
...
5
6
7
>>> nombres = [5, 6, 7]
>>> for x in nombres:
...     print(x)
...
5
6
7
```

* [Indices ou valeurs](https://codex.forge.apps.education.fr/exercices/indices_valeurs/)

* [Nombre de répétitions](https://codex.forge.apps.education.fr/exercices/repetitions/)

* [Moyenne simple](https://codex.forge.apps.education.fr/exercices/moyenne/)

* [Dernière occurrence](https://codex.forge.apps.education.fr/exercices/derniere_occurrence/)


## Approfondissement

```pycon
>>> nombres = [5, 6, 7]
>>> [x for x in nombres if x % 2 == 0]
[6]
```

* [Filtrer un tableau](https://codex.forge.apps.education.fr/en_travaux/filtre_tableau/)






