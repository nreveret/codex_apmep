# Algorithmique dans les programmes de Mathématiques

## Seconde générale

### Programme d'algorithmique

* Utiliser les variables et les instructions élémentaires
  * Contenus
    * Variables informatiques de type entier, booléen, flottant, chaîne de caractères.
    * Affectation (notée ← en langage naturel).
    * Séquence d'instructions.
    * Instruction conditionnelle.
    * Boucle bornée (for), boucle non bornée (while).
  * Capacités attendues
    * Choisir ou déterminer le type d'une variable (entier, flottant ou chaîne de caractères).
    * Concevoir et écrire une instruction d'affectation, une séquence d'instructions, une instruction conditionnelle.
    * Écrire une formule permettant un calcul combinant des variables.
    * Programmer, dans des cas simples, une boucle bornée, une boucle non bornée.
    * Dans des cas plus complexes : lire, comprendre, modifier ou compléter un algorithme ou un programme.

* Notion de fonction
  * Contenus
    * Fonctions à un ou plusieurs arguments.
    * Fonction renvoyant un nombre aléatoire. Série statistique obtenue par la répétition de l'appel d'une telle fonction.
  * Capacités attendues
    * Écrire des fonctions simples ; lire, comprendre, modifier, compléter des fonctions plus complexes. Appeler une fonction.
    * Lire et comprendre une fonction renvoyant une moyenne, un écart type. Aucune connaissance sur les listes n'est exigée.
    * Écrire des fonctions renvoyant le résultat numérique d'une expérience aléatoire, d'une répétition d'expériences aléatoires indépendantes.

### Exemples d'algorithmes

* Manipuler les nombres réels
    * Déterminer par balayage un encadrement de $\sqrt2$ d'amplitude inférieure ou égale à $10^{-n}$.

* Utiliser les notions de multiple, diviseur et de nombre premier
  * Déterminer si un entier naturel $a$ est multiple d'un entier naturel $b$.
  * Pour des entiers $a$ et $b$ donnés, déterminer le plus grand multiple de $a$ inférieur ou égal à $b$
  * Déterminer si un entier naturel est premier

* Utiliser le calcul littéral
  * Déterminer la première puissance d'un nombre positif donné supérieure ou inférieure à une valeur donnée

* Représenter et caractériser les droites du plan
  * Étudier l'alignement de trois points dans le plan
  * Déterminer une équation de droite passant par deux points donnés

* Étudier les variations et les extremums d'une fonction
  * Pour une fonction dont le tableau de variations est donné, algorithmes d'approximation numérique d'un extremum (balayage, dichotomie)
  * Algorithme de calcul approché de longueur d'une portion de courbe représentative de fonction

