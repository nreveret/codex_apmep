# Algorithmique dans les programmes de Mathématiques

## Première - Enseignement de spécialité

### Programme d'algorithmique

La démarche algorithmique est, depuis les origines, une composante essentielle de l'activité mathématique. Au collège, en mathématiques et en technologie, les élèves ont appris à écrire, mettre au point et exécuter un programme simple. La classe de seconde a permis de consolider les acquis du cycle 4 autour de deux idées essentielles :

* la notion de fonction ;
* la programmation comme production d'un texte dans un langage informatique.

L'enseignement de spécialité de mathématiques de classe de première vise la consolidation des notions de variable, d'instruction conditionnelle et de boucle ainsi que l'utilisation des fonctions. La seule notion nouvelle est celle de liste qui trouve naturellement sa place dans de nombreuses parties du programme et aide à la compréhension de notions mathématiques telles que les suites numériques, les tableaux de valeurs, les séries statistiques…

Comme en classe de seconde, les algorithmes peuvent être écrits en langage naturel ou utiliser le langage Python.

Les notions relatives aux types de variables et à l'affectation sont consolidées. Comme en classe de seconde, on utilise le symbole « ← » pour désigner l'affection dans un algorithme écrit en langage naturel.

L'accent est mis sur la programmation modulaire qui permet de découper une tâche complexe en tâches plus simples.

#### Histoire des mathématiques

De nombreux textes témoignent d'une préoccupation algorithmique au long de l'Histoire. Lorsqu'un texte historique a une visée algorithmique, transformer les méthodes qu'il présente en un algorithme, voire en un programme, ou inversement, est l'occasion de
travailler des changements de registre qui donnent du sens au formalisme mathématique.

#### Notion de liste

La génération des listes en compréhension et en extension est mise en lien avec la notion d'ensemble. Les conditions apparaissant dans les listes définies en compréhension permettent de travailler la logique. Afin d'éviter des confusions, on se limite aux listes sans
présenter d'autres types de collections.

Capacités attendues

* Générer une liste (en extension, par ajouts successifs ou en compréhension).
* Manipuler des éléments d'une liste (ajouter, supprimer…) et leurs indices.
* Parcourir une liste.
+ Itérer sur les éléments d'une liste.

### Exemples d'algorithmes

* Suites numériques, modèles discrets
    * Calcul de termes d'une suite, de sommes de termes, de seuil.
    * Calcul de factorielle.
    * Liste des premiers termes d'une suite : suites de Syracuse, suite de Fibonacci.

* Dérivation
  * Écrire la liste des coefficients directeurs des sécantes pour un pas donné.

* Variations et courbes représentatives des fonctions
  * Méthode de Newton, en se limitant à des cas favorables.
  
* Fonction exponentielle
  * Construction de l'exponentielle par la méthode d'Euler.
  * Détermination d'une valeur approchée de e à l'aide de la suite $u_n=\left(1+\frac1n\right)^n$.

* Fonctions trigonométriques
  * Approximation de $\pi$ par la méthode d'Archimède.

* Probabilités conditionnelles et indépendance
  * Méthode de Monte-Carlo : estimation de l'aire sous la parabole, estimation du nombre $\pi$.

* Variables aléatoires réelles
  * Algorithme renvoyant l'espérance, la variance ou l'écart type d'une variable aléatoire.
  * Fréquence d'apparition des lettres d'un texte donné, en français, en anglais.