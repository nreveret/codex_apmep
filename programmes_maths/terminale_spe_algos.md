# Algorithmique dans les programmes de Mathématiques

## Terminale - Enseignement de spécialité

### Programme d'algorithmique

La démarche algorithmique est, depuis les origines, une composante essentielle de l'activité mathématique. Au collège, en mathématiques et en technologie, les élèves ont appris à écrire, mettre au point et exécuter un programme simple. Les classes de seconde et de première ont permis de consolider les acquis du collège (notion de variable, type, de variables, affectation, instruction conditionnelle, boucle notamment), d'introduire et d'utiliser la notion de fonction informatique et de liste. En algorithmique et programmation, le programme reprend les programmes de seconde et de première sans introduire de notion nouvelle, afin de consolider le travail des classes précédentes.

Les algorithmes peuvent être écrits en langage naturel ou utiliser le langage Python. On utilise le symbole « ← » pour désigner l'affection dans un algorithme écrit en langage naturel. L'accent est mis sur la programmation modulaire qui permet de découper une tâche complexe en tâches plus simples.

L'algorithmique trouve naturellement sa place dans toutes les parties du programme et aide à la compréhension et à la construction des notions mathématiques.

#### Histoire des mathématiques

De nombreux textes témoignent d'une préoccupation algorithmique au long de l'Histoire. Lorsqu'un texte historique a une visée algorithmique, transformer les méthodes qu'il présente en un algorithme, voire en un programme, ou inversement, est l'occasion de travailler des changements de registre qui donnent du sens au formalisme mathématique.

#### Notion de liste

La génération des listes en compréhension et en extension est mise en lien avec la notion d'ensemble. Les conditions apparaissant dans les listes définies en compréhension permettent de travailler la logique. Afin d'éviter des confusions, on se limite aux listes sans présenter d'autres types de collections.

Capacités attendues :

* Générer une liste (en extension, par ajouts successifs ou en compréhension).
* Manipuler des éléments d'une liste (ajouter, supprimer…) et leurs indices.
* Parcourir une liste.
* Itérer sur les éléments d'une liste.

### Exemples d'algorithmes

* Combinatoire et dénombrement
  * Pour un entier n donné, génération de la liste des coefficients binomiaux à l'aide de la relation de Pascal.
  * Génération des permutations d'un ensemble fini, ou tirage aléatoire d'une permutation.
  * Génération des parties à 2, 3 éléments d'un ensemble fini.

* Suites
  * Recherche de seuils.
  * Recherche de valeurs approchées de $\pi$, $\text{e}$, $\sqrt2$, $\frac{1+\sqrt5}{2}$ , $\ln(2)$, etc.

* Continuité des fonctions d'une variable réelle
  * Méthode de dichotomie.
  * Méthode de Newton, méthode de la sécante.

* Fonction logarithme
  * Algorithme de Briggs pour le calcul du logarithme.

* Primitives, équations différentielles
  * Résolution par la méthode d'Euler de $y'=f$ et $y'=ay+b$.

* Calcul intégral
  * Méthodes des rectangles, des milieux, des trapèzes.
  * Méthode de Monte-Carlo.
  * Algorithme de Brouncker pour le calcul de $\ln(2)$.

* Succession d'épreuves indépendantes, schéma de Bernoulli
  * Simulation de la planche de Galton.
  * Problème de la surréservation. Étant donné une variable aléatoire binomiale $X$ et un réel strictement positif $\alpha$, détermination du plus petit entier $k$ tel que $P(X > k) \leqslant \alpha$.
  * Simulation d'un échantillon d'une variable aléatoire.

* Concentration, loi des grands nombres
  * Calculer la probabilité de $(\lvert S_n - pn\rvert > \sqrt{n} )$, où $S_n$ est une variable aléatoire qui suit une loi binomiale $B(n,p)$. Comparer avec l'inégalité de Bienaymé-Tchebychev.
  * Simulation d'une marche aléatoire.
  * Simuler $N$ échantillons de taille $n$ d'une variable aléatoire d'espérance $\mu$ et d'écart-type $\sigma$. Calculer l'écart type $s$ de la série des moyennes des échantillons observés, à comparer à $\frac{\sigma}{\sqrt{n}}$. Calculer la proportion des échantillons pour lesquels l'écart entre la moyenne et $\mu$ est inférieur ou égal à $ks$, ou à $k\sigma / \sqrt{n}$, pour $k = 1, 2, 3$.